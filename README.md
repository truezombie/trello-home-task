This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

# Chat Simulator

Write a client-side "chat simulator" that takes the URL of a JSON chat transcript, and displays a replay of the chat.  

Here's a transcript you can use to test and run your simulator:

```
[
  { delta: 1000, payload: { type: 'message', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' }, message: { id: 1, text: "Hello!" } }},
  { delta: 2000, payload: { type: 'message', user: { id: 2, user_name: 'chorizo', display_name: 'Chorizo' }, message: { id: 2, text: "Hi Taco!" } }},
  { delta: 2100, payload: { type: 'connect', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' } }},
  { delta: 3000, payload: { type: 'message', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' }, message: { id: 3, text: "Hi Taco!" } }},
  { delta: 4000, payload: { type: 'message', user: { id: 1, user_name: 'pete', display_name: 'Pete the Computer' }, message: { id: 4, text: "What's going on in here?" } }},
  { delta: 5000, payload: { type: 'message', user: { id: 2, user_name: 'chorizo', display_name: 'Chorizo' }, message: { id: 5, text: "We're testing this chat replay app" } }},
  { delta: 6000, payload: { type: 'message', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' }, message: { id: 6, text: "Seems like it's working fine … in the *simple* case :)" } }},
  { delta: 6500, payload: { type: 'message', user: { id: 2, user_name: 'chorizo', display_name: 'Chorizo' }, message: { id: 7, text: "Cool!" } }},
  { delta: 6600, payload: { type: 'update', user: { id: 2, username: 'chorizothecat', display_name: 'Chorizo the Cat' }}},
  { delta: 7000, payload: { type: 'update', message: { id: 6, text: "Seems like it's working find … for *edits* also" }}},
  { delta: 8000, payload: { type: 'message', user: { id: 2, username: 'chorizothecat', display_name: 'Chorizo the Cat' }, message: { id: 8, text: "Just following @pete's lead…" }}},
  { delta: 8250, payload: { type: 'message', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' }, message: { id: 9, text: "We _know_ you're a cat @chorizothecat :facepalm:" }}},
  { delta: 10000, payload: { type: 'update', user: { id: 2, username: 'chorizo', display_name: 'Chorizo' }}},
  { delta: 11000, payload: { type: 'message', user: { id: 2, user_name: 'chorizo', display_name: 'Chorizo' }, message: { id: 10, text: "Oh fine … I mostly just wanted to see what happened when I changed my profile.  Sorry @taco :scream_cat:" } }},
  { delta: 12000, payload: { type: 'delete', message: { id: 9 }}},
  { delta: 14000, payload: { type: 'delete', message: { id: 10 }}},
  { delta: 15000, payload: { type: 'message', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' }, message: { id: 11, text: "Well, _that_ was fun.  Changing the subject … have you seen https://en.wikipedia.org/wiki/Market_share_of_personal_computer_vendors?" } }},
  { delta: 20000, payload: { type: 'message', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' }, message: { id: 12, text: "_I_ thought it was pretty interesting.\n\nReally makes you *think*." } }},
  { delta: 30000, payload: { type: 'message', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' }, message: { id: 13, text: "Anyone…?  @taco?  @chorizo?" } }},
  { delta: 31000, payload: { type: 'disconnect', user: { id: 2, user_name: 'chorizo', display_name: 'Chorizo' } }},
  { delta: 31002, payload: { type: 'disconnect', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' } }},
  { delta: 32000, payload: { type: 'message', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' }, message: { id: 14, text: "Aww :sob:" } }},
  { delta: 33100, payload: { type: 'connect', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' } }},
  { delta: 33150, payload: { type: 'disconnect', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' } }},
  { delta: 34000, payload: { type: 'connect', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' } }},
  { delta: 36000, payload: { type: 'message', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' }, message: { id: 15, text: "Whoops, I'm back.  Flaky wireless connection :ghost:…"} }},
  { delta: 36100, payload: { type: 'disconnect', user: { id: 1, user_name: 'taco', display_name: 'Taco Spolsky' } }},
  { delta: 66000, payload: { type: 'disconnect', user: { id: 3, user_name: 'pete', display_name: 'Pete the Computer' } }}
]
```

- You can use the `delta` values to know when different events are meant to occur.
- You can assume that the GET request for the URL will respond with appropriate CORS headers.
- You've been given access to a repo on Bitbucket that you should push your code to.
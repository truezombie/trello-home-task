import { messageTypes } from './types';

// TODO: need to refactor
export const getPreparedForRenderMessages = (messages) => {
  return messages.reduce((acc, curr) => {
    const { message, type } = curr.payload;

    const isMessageDeleted = type === messageTypes.delete;
    const isMessageUpdated = type === messageTypes.update && message;

    if (isMessageDeleted) {
      return acc.map((item) => {
        return item.payload?.message?.id === message?.id
          ? {
              ...item,
              payload: {
                ...item.payload,
                type: 'delete',
                message: { ...curr.payload.message, text: '' },
              },
            }
          : item;
      });
    }

    if (isMessageUpdated) {
      return acc.map((item) => {
        return item.payload?.message?.id === message?.id
          ? {
              ...item,
              payload: { ...item.payload, type: 'update', message },
            }
          : item;
      });
    }

    if (!isMessageDeleted && !isMessageUpdated) {
      acc.push(curr);
    }

    return acc;
  }, []);
};

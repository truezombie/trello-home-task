export const messageTypes = {
  disconnect: 'disconnect',
  message: 'message',
  connect: 'connect',
  update: 'update',
  delete: 'delete',
};

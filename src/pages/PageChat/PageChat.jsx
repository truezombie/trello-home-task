import React from 'react';
import PropTypes from 'prop-types';

import { messageTypes } from '../../utils/types';
import { Message, Disconnect, Connect, UserDataUpdate } from '../../components';

const PageChat = ({ messages }) => {
  // TODO: need to refactor this method
  const getMessageComponentByType = ({ payload: { type, message, user } }) => {
    const isDelete = type === messageTypes.delete;
    const isMessage = type === messageTypes.message;
    const isConnect = type === messageTypes.connect;
    const isDisconnect = type === messageTypes.disconnect;
    const isMessageUpdated = type === messageTypes.update && message;
    const isUserDataUpdated = type === messageTypes.update && user && !message;

    if (isDelete) {
      return (
        <Message name={user?.display_name} message={message.text} isDelete />
      );
    }

    if (isMessage) {
      return <Message name={user?.display_name} message={message.text} />;
    }

    if (isDisconnect) {
      return <Disconnect name={user.display_name} />;
    }

    if (isConnect) {
      return <Connect name={user?.display_name} />;
    }

    if (isMessageUpdated) {
      return (
        <Message name={user?.display_name} message={message.text} isUpdate />
      );
    }

    if (isUserDataUpdated) {
      return <UserDataUpdate name={user?.display_name} />;
    }

    return null;
  };

  return (
    <>
      {messages.map((item) => {
        return <div key={item.delta}>{getMessageComponentByType(item)}</div>;
      })}
    </>
  );
};

PageChat.propTypes = {
  messages: PropTypes.array.isRequired,
};

export default PageChat;

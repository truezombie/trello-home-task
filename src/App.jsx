import React from 'react';

import Chat from './containers/Chat';

import { MOCKED_MESSAGES } from './utils/dataMock';

import './index.css';

function App() {
  return <Chat messages={MOCKED_MESSAGES} />;
}

export default App;

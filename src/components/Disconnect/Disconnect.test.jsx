import React from 'react';
import { shallow } from 'enzyme';

import Disconnect from './Disconnect';

const setUp = (props) => shallow(<Disconnect {...props} />);

describe('<Disconnect />', () => {
  it('should render Disconnect component', () => {
    const wrapper = setUp({ name: 'test' });

    expect(wrapper.html()).toMatchSnapshot();
  });
});

import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Disconnect = ({ name }) => {
  return (
    <p className='disconnect'>
      <small>{`User ${name}. Was disconnected.`}</small>
    </p>
  );
};

Disconnect.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Disconnect;

import React from 'react';
import { shallow } from 'enzyme';

import UserDataUpdate from './UserDataUpdate';

const setUp = (props) => shallow(<UserDataUpdate {...props} />);

describe('<Message />', () => {
  it('should render UserDataUpdate component', () => {
    const wrapper = setUp({ name: 'test' });

    expect(wrapper.html()).toMatchSnapshot();
  });
});

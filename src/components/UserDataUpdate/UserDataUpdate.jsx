import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const UserDataUpdate = ({ name }) => {
  return (
    <p className='userDataUpdate'>
      <small>{`User ${name} updated profile data`}</small>
    </p>
  );
};

UserDataUpdate.propTypes = {
  name: PropTypes.string.isRequired,
};

export default UserDataUpdate;

import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Connect = ({ name }) => {
  return (
    <p className='connect'>
      <small>{`User ${name}. Was connected.`}</small>
    </p>
  );
};

Connect.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Connect;

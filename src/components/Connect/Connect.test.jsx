import React from 'react';
import { shallow } from 'enzyme';

import Connect from './Connect';

const setUp = (props) => shallow(<Connect {...props} />);

describe('<Connect />', () => {
  it('should render Connect component', () => {
    const wrapper = setUp({ name: 'test' });

    expect(wrapper.html()).toMatchSnapshot();
  });
});

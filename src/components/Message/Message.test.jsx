import React from 'react';
import { shallow } from 'enzyme';

import Message from './Message';

const setUp = (props) => shallow(<Message {...props} />);

describe('<Message />', () => {
  it('should render Message component', () => {
    const wrapper = setUp({ message: 'test', name: 'test' });

    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should render Message component with update message', () => {
    const wrapper = setUp({ update: true, message: 'test', name: 'test' });

    expect(wrapper.html()).toMatchSnapshot();
  });
});

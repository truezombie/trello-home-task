import React from 'react';
import ReactMarkdown from 'react-markdown';
import PropTypes from 'prop-types';
import emoji from 'emoji-dictionary';

import './style.css';

const Message = ({ message, name, isDelete, isUpdate }) => {
  const emojiSupport = (text) =>
    text.value.replace(/:\w+:/gi, (name) => emoji.getUnicode(name));

  return (
    <div className='message'>
      <b>{name}</b>{' '}
      {isUpdate && !isDelete ? (
        <small>
          <i>(Message was updated)</i>
        </small>
      ) : null}
      {isDelete ? (
        <p className='deleted-message'>Message wad deleted!</p>
      ) : (
        <ReactMarkdown source={message} renderers={{ text: emojiSupport }} />
      )}
    </div>
  );
};

Message.propTypes = {
  isUpdate: PropTypes.bool,
  isDelete: PropTypes.bool,
  name: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
};

Message.defaultProps = {
  isUpdate: false,
  isDelete: false,
};

export default Message;

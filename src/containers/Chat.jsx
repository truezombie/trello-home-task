import React, { useMemo } from 'react';
import PropTypes from 'prop-types';

import { PageChat } from '../pages';

import { getPreparedForRenderMessages } from '../utils/dataPrepare';

const Chat = ({ messages }) => {
  const preparedMessages = useMemo(() => {
    return getPreparedForRenderMessages(messages);
  }, [messages]);

  return <PageChat messages={preparedMessages} />;
};

Chat.propTypes = {
  messages: PropTypes.array.isRequired,
};

export default Chat;
